# https://wiki.archlinux.org/index.php/proxy_settings
function proxy(){
    echo -n "username:"
    read -e username
    echo -n "password:"
    read -es password
    export http_proxy="http://$username:$password@proxyserver:8080/"
    export https_proxy=$http_proxy
    export ftp_proxy=$http_proxy
    export rsync_proxy=$http_proxy
    export no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com"
    echo -e "\nProxy environment variable set."
}
function proxyoff(){
    unset HTTP_PROXY
    unset http_proxy
    unset HTTPS_PROXY
    unset https_proxy
    unset FTP_PROXY
    unset ftp_proxy
    unset RSYNC_PROXY
    unset rsync_proxy
    echo -e "\nProxy environment variable removed."
} 


assignProxy(){
  PROXY_ENV="http_proxy ftp_proxy https_proxy all_proxy no_proxy HTTP_PROXY HTTPS_PROXY FTP_PROXY NO_PROXY ALL_PROXY"
  for envar in $PROXY_ENV
  do
    export $envar=$1
  done
}

clrProxy(){
  assignProxy "" # This is what 'unset' does.
}

myProxy(){
  user=YourUserName
  read -p "Password: " -s pass &&  echo -e " "
  proxy_value="http://$user:$pass@ProxyServerAddress:Port"
  assignProxy $proxy_value  
}
 
