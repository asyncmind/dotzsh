#-*- mode: sh; ... -*-
_force_rehash() {
 (( CURRENT == 1 )) && rehash
 return 1	# Because we didn't really complete anything
}

sviw(){
	sudo vim `which $1`
}	

viw(){
	vim `which $1`
}	

function git_diff() {
  git diff --no-ext-diff -w "$@" | vim -R -
}
function log_command(){
    history |tail -n1 >> .command_log
}
# Set terminal title
# @param string $1  Tab/window title
# @param string $2  (optional) Separate window title
# The latest version of this software can be obtained here:
# http://fvue.nl/wiki/NameTerminal

function nameTerminal() {
    [ "${TERM:0:5}" = "xterm" ]   && local ansiNrTab=0
    [ "$TERM"       = "rxvt-256color" ]    && local ansiNrTab=61
    [ "$TERM"       = "konsole" ] && local ansiNrTab=30 ansiNrWindow=0
        # Change tab title
    [ $ansiNrTab ] && echo -n $'\e'"]$ansiNrTab;$1"$'\a'
        # If terminal support separate window title, change window title as well
    [ $ansiNrWindow -a "$2" ] && echo -n $'\e'"]$ansiNrWindow;$2"$'\a'
} # nameTerminal()

function _test_completion(){
reply=(`ls  ~/www/`);
}
function test_completion(){
echo "ok"
}
compctl -K _test_completion  test_completion



function emacs_session(){
exec emacsclient -c -a "" -s $1 
}

function keyboard_config(){
    if [ $DISPLAY ]; then
        xset r rate 150 40
        xmodmap ~/.xmodmap
        setxkbmap -option 'ctrl:swapcaps'
        #sudo fn_toggle
    fi
}

function reset_bluetooth(){
    rfkill unblock bluetooth
    hciconfig hci0 reset
}

logiconnect(){
    #echo connect 00:1F:20:75:A3:9D | bluetoothctl
    keyboard_config
   sleep 5
    sudo k810-conf -d /dev/hidraw[0,1,2,3] -f off
}


function duf {
du -sk "$@" | sort -n | while read size fname; do for unit in k M G T P E Z Y; do if [ $size -lt 1024 ]; then echo -e "${size}${unit}\t${fname}"; break; fi; size=$((size/1024)); done; done
}

xe(){ curl "http://www.xe.com/wap/2co/convert.cgi?Amount=$1&From=$2&To=$3" -A "Mozilla" -s | sed -n "s/.*>\(.*\) $3<.*/\1/p";}

function postexec(){
    # Restore tmux-title to 'zsh'
    printf "\033k$(tmux display-message -p '#W')-\033\\"
    # Restore urxvt-title to 'zsh'
    print -Pn "\e]2;zsh:%~\a"
}
function preexec(){
    # set tmux-title to running program
    printf "\033k$(echo "$1" | cut -d" " -f1)\033\\"
    # set urxvt-title to running program
    print -Pn "\e]2;zsh:$(echo "$1" | cut -d" " -f1)\a"
}

reset_null(){
sudo mknod /dev/newnull c 1 3
sudo chmod 777 /dev/newnull
sudo mv -f /dev/newnull /dev/null
}
