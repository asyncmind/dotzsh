#
# Executes commands at login pre-zshrc.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

export PATH=~/.npm-packages/bin:~/.emacs.d/bin/:~/.local/bin:~/.zsh/bin/:/usr/sbin/:/sbin/:~/.npm/bin/:~/.bin:$PATH

export SUDO_EDITOR="/usr/bin/vim -p -X"
export HISTSIZE=10000
export SAVEHIST=10000
export EDITOR=vim
export AUTOSSH_POLL=5


export PYTHONSTARTUP=~/.local/lib/python2.7/pythonstartup.py

#export XDG_MENU_PREFIX=""
#export XDG_CONFIG_HOME="${HOME}/.config"
#export XDG_DATA_HOME="${HOME}/.local/share"
#
#export XDG_CACHE_HOME="${HOME}/.cache"
#if [ ! -z $XDG_DATA_DIRS ]; then
#  export XDG_DATA_DIRS=$XDG_DATA_DIRS:/usr/share:/usr/local/share
#else
#  export XDG_DATA_DIRS="/usr/share:/usr/local/share"
#fi
#if [ ! -z $XDG_CONFIG_DIRS ]; then
#  export XDG_CONFIG_DIRS=$XDG_CONFIG_DIRS:/etc/xdg
#else
#  export XDG_CONFIG_DIRS="/etc/xdg"
#fi
#
#export XDG_RUNTIME_DIR=/run/user/1000

#export KDE_SESSION_ID=1
export XDG_CURRENT_DESKTOP=KDE
export QT_QPA_PLATFORMTHEME=qt5ct

export CPM_USE_NODE=1
export PACKAGES_PATH=~/www/var/lib/

export TERM=xterm-256color
export TZ=Australia/Sydney
export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
export ZSH_CACHE_DIR=~/.cache/zsh/
mkdir -p $ZSH_CACHE_DIR
